import { Controller, Get, Post, Body, Param, Put, Delete } from '@nestjs/common';
// api文档
import { ApiTags, ApiOperation, ApiProperty } from '@nestjs/swagger'
// 数据库模型
// import { PostModel } from './posts.model';
// 验证管道
import { IsNotEmpty } from 'class-validator'
import { InjectModel } from 'nestjs-typegoose';

import { Post as PostSchema } from './posts.model';

// 数据传输对象（数据约束规范）DTO = > Data Transform Operation
// 类型声明全是小写！！！！
// class里面无逗号区分
class CreatePostDto {
    @ApiProperty({ description: '帖子标题', example: '帖子标题示例' })
    // 装饰器表示 title 不能为空，如果为空则返回 message 内容
    @IsNotEmpty({message:'请填写标题'})
    title: string
    @ApiProperty({description:'帖子内容',example:'帖子内容示例'})
    content:string
}




@ApiTags('默认')
@Controller('posts')
export class PostsController {


    constructor(
        // private  : 表示私有的
        // readonly : 表示只读的
        // InjectModel 依赖注入模型：底层框架会自动找出 PostSchema 类并解析，并且自动实例化（new操作），然后赋值给 postModel
        @InjectModel(PostSchema) private readonly postModel
    ){}



    @Get('/')
    @ApiOperation({summary: '显示博客列表'})
    async index() {
        return await this.postModel.find()
    }


    // 参数顺序不重要！！！
    @ApiOperation({summary: '创建帖子'})
    @Post()
    async create(@Body() createPostDto: CreatePostDto) {
        await this.postModel.create(createPostDto)
        return {
            success: true
        }
    }


    @ApiOperation({summary: '博客详情'})
    @Get(':id')
    async detail(@Param('id') id: string) {
        return await this.postModel.findById(id)
    }


    @ApiOperation({summary: '编辑帖子'})
    @Put(':id')
    async update(@Param('id') id: string, @Body() updatePostDto: CreatePostDto) {
        await this.postModel.findByIdAndUpdate(id,updatePostDto)
        return {
            success: true,
        }
    }
    


    @ApiOperation({summary: '删除帖子'})
    @Delete(':id')
    async  remove(@Param('id') id: string) {
        await this.postModel.findByIdAndDelete(id)
        return {
            success: true
        }
    }
    
}
