import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';


// 链接数据库
// import * as mongoose from 'mongoose'

// Swagger 接口文档
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {

  // 建立数据库连接
  // mongoose.connect('mongodb://localhost/nest-blog-api', {
  //   useNewUrlParser: true,
  //   useFindAndModify: false,
  //   useCreateIndex:true
  // })

  // 创建APP 实例
  const app = await NestFactory.create(AppModule);


  // pipe 管道
  app.useGlobalPipes(new ValidationPipe)

  // Swagger 接口文档相关配置信息
  const options = new DocumentBuilder()
    .setTitle('NestJs 博客API')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .addTag('cats')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  // 接口文档挂载路径
  SwaggerModule.setup('api-docs', app, document);




  await app.listen(3000);
}
bootstrap();
