# NestJs-Curd

*本项目是学习NestJs的好demo*！

> NestJs-Curd是一个基于课程的demo，提供NestJs方式快速实现CURD操作接口。轻松连接数据库建立接口测试文档，从而使得服务端开发更加高效！

## 课程目录

- [x] P1  NestJs开发博客API-1-基础项目搭建
- [x] P2  NestJs开发博客API-2-创建帖子模块和控制器
- [x] P3  NestJs开发博客API-3-使用Swagger自动生成接口文档
- [x] P4  NestJs开发博客API-4-Post请求和数据传输对象DTO
- [x] P5  NestJs开发博客API-5-Put、Delete请求和URL路径参数
- [x] P6  NestJs开发博客API-6-Typegoose模型定义
- [x] P7  NestJs开发博客API-7-7分钟完成帖子CRUD操作API
- [x] P8  NestJs开发博客API-8-使用class-validator验证请求数据
- [x] P9  NestJs开发博客API-9-使用nestjs-typegoose依赖注入Post模型
- [ ] P10 NestJs开发博客API-10-使用nestjs-mongoose-crud快速实现CRUD

## 课程信息

1. 课程作者 ：全栈之巅
2. 平    台 ：哔哩哔哩
3. 番    号 ：av66475543

## 依赖说明

``` JavaScript
{
    "@hasezoey/typegoose": "^6.0.0-32",
    "@nestjs/common": "^6.10.14",
    "@nestjs/core": "^6.10.14",
    "@nestjs/platform-express": "^6.10.14",
    "@nestjs/swagger": "^4.2.9",
    "@types/mongoose": "^5.7.1",
    "class-transformer": "^0.2.3",
    "class-validator": "^0.11.0",
    "mongoose": "^5.9.1",
    "nestjs-typegoose": "^7.1.0",
    "reflect-metadata": "^0.1.13",
    "rimraf": "^3.0.0",
    "rxjs": "^6.5.4",
    "swagger-ui-express": "^4.1.3"
}
```


## 安装依赖

```bash
$ npm install
```
或者

```bash
$ yarn
```

## 运行项目

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```


## 测试

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```


## License

  NestJs-Curd is [MIT licensed](LICENSE).
